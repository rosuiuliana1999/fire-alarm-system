# Fire Alarm System

## Functionalitate:

- Atunci cand nu se detecteaza foc: LED-ul verde aprins, Buzzer-ul si LED-ul rosu oprite, pe LCD se afiseaza mesaj cu situatia actuala.

- Atunci cand se detecteaza foc: LED-ul rosu aprins, cel verde stins, Buzzer-ul pornit,
pe LCD se afiseara un mesaj de alerta, iar prin difuzor se va reda un fisier audio de alarmare.

## Componente folosite:

- Arduino Uno
- BreadBoard
- Wires
- Modul senzor flacara
   - Unghi detectie: 60 grade
   - Pentru surse de foc si lumina cu lungimi de unda cuprinde intre 760 si 1100nm
   - Foloseste un fototranzistor ce functioneaza pe baza de radiatie infrarosie
- Buzzer
- LED rosu si LED verde
- Ecran LCD 1602 IIC/I2C
- Difuzor
- Tranzistor
   - pentru amplificare

Pentru redarea fisierului audio am folosit PCM Library. Pentru ca avea o dimensiune destul de mare pentru a fi incarcat pe Arduino, am folosit Audacity Software pentru a ii reduce dimensiunea: din 48000Hz Project Rate la 8000Hz, iar formatul din 32-bit PCM in 16-bit PCM.
Pentru conversia fisierului audio in cod am folosit un software EncodeAudio 

## Schema electrica:

![Semantic description of image](/Imagini/schema.png "Schema electrica")

## Montajul realizat:

![Semantic description of image](/Imagini/schema2edit.jpeg "Montaj")

## Link catre videoclip: 

https://www.youtube.com/watch?v=EHzK2FfCOVU


## Proiecte similare:

1. **Fire Detection Using Arduino and Flame Sensor:** 

https://www.instructables.com/Flame-detection-using-Arduino-and-flame-sensor/

Componente: 	
   1.Flame senzor
   2.Arduino
   3.Bread board
   4.LED
   5.Buzzer
   6.Connecting wires

Functionalitate: Limita de 100 pentru senzorul de foc. Cand flacara se apropie de 
senzor LED ul se aprinde si Buzzer-ul porneste,
cand se indeparteaza, acestea se opresc.

2. **Flame Detector With Arduino:** 

https://www.instructables.com/Flame-Detector-With-Arduino/

Componente: 	
   1.Flame detector
   2.Arduino Uno
   3.Buzzer
   4.Dupont Wire
Descriere senzor: ky-26 flame sensor module - permite detectarea existentei arderii de catre lumina emisa de foc
sensibilitatea la foc se poate modifica
flacara unei brichete a fost testata la o distanta de 80cm
Functionalitate similara, limita pentru senzor setata la 500.

3. **Interfacing Flame Sensor with Arduino to Build a Fire Alarm System:**

https://circuitdigest.com/microcontroller-projects/arduino-flame-sensor-interfacing

Componente: 	
   1.Arduino Uno	 
   2.Flame sensor module
   3.LED
   4.Buzzer
   5.Resistor
   6.Jumper wires
Functionalitate: IR Flame sensor - YG1006 sensor - are o fotodioda IR care detecteaza lumina IR, emisa de foc
iersire 1 logic daca detecteaza foc, daca nu 0 logic. Pentru fiecare caz se afiseaza diferite mesaje si porneste 
sau nu LED ul si buzzer-ul
